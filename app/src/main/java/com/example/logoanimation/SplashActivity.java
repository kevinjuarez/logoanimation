package com.example.logoanimation;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class SplashActivity extends AppCompatActivity {
    // Initialize variable

    ImageView ivtop, ivheart, ivbeat, ivbottom;
    TextView textView;
    CharSequence charSequence;
    int index;
    long delay = 50;
    Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_splash);
        //Assign variable

        ivtop = findViewById(R.id.iv_top);
        ivheart = findViewById(R.id.iv_heart);
        ivbeat = findViewById(R.id.iv_beat);
        ivbottom = findViewById(R.id.iv_bottom);
        textView = findViewById(R.id.text_view);

        // Set full Screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //initialize top animation
        Animation animationtop = AnimationUtils.loadAnimation(this, R.anim.top_wave);

        // Start top animation
        ivtop.setAnimation(animationtop);

        //Initialize object animator
        ObjectAnimator objectAnimator= ObjectAnimator.ofPropertyValuesHolder(
              ivheart,
              PropertyValuesHolder.ofFloat("scaleX", 1.2f),
              PropertyValuesHolder.ofFloat("scaleY",1.2f)
        );

        // Set duration
        objectAnimator.setDuration(50);

        // Set repeat count
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);

        // Set repeat mode
        objectAnimator.setRepeatMode(ValueAnimator.REVERSE);

        // Start animator
        objectAnimator.start();

        // Set animate text
        animatText("Heart Beat");

        //Load GIF
        Glide.with(this).load("https://firebasestorage.googleapis.com/v0/b/demoapp-ae96a.appspot.com/o/heart_beat.gif?alt=media&token=b21dddd8-782c-457c-babd-f2e922ba172b")
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivbeat);

        // Initialize Bottom animation
        Animation animationbottom = AnimationUtils.loadAnimation(this,R.anim.bottom_wave);

        // Start bottom animation
        ivbottom.setAnimation(animationbottom);

        // Initialize handler
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Redirect to main
                startActivity(new Intent(SplashActivity.this
                ,MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                // Finish
                finish();
            }
        },4000);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // When runnable id run, set text

            textView.setText(charSequence.subSequence(0,index++));

            //check condition
            if (index <= charSequence.length()){
                // When index is equals to text length, Run handler
                handler.postDelayed(runnable,delay);
            }
        }
    };

    //Create animated text method
    public void animatText(CharSequence cs){
        // Set text
        charSequence = cs;

        // Clear index
        index = 0;

        // Clear Text
        textView.setText("");

        // Remove call back
        handler.removeCallbacks(runnable);

        //Run Hadler
        handler.postDelayed(runnable,delay);
    }
}
